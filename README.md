# Agent en intervention - Mobile

[![pipeline status](https://gitlab.com/incubateur-territoires/startups/agents-intervention/mobile/badges/main/pipeline.svg)](https://gitlab.com/incubateur-territoires/startups/agents-intervention/mobile/-/commits/main)

## Installation

**Pour utiliser cette application, il faut au préalable installer l'[API](https://gitlab.com/incubateur-territoires/startups/agents-intervention/api) du projet.**

### Télécharger le projet :

```shellsession
user@host ~$ cd [CHEMIN_OU_METTRE_LE_PROJET] # Exemple : ~/projets/
user@host projets$ git clone https://gitlab.com/incubateur-territoires/startups/agents-intervention/mobile.git
user@host projets$ cd mobile
```

### Ajouter les variables d'environnement

| Nom de la variable | Valeur par défaut | Exemple de valeur | Présente dans le fichier | Documentation |
|----|----|----|----|----|
| NUXT_API_URL | 'http://nginx/' | 'http://nginx/' | ./.env.dist, ./.env | [doc](https://gitlab.com/incubateur-territoires/startups/agents-intervention/mobile#configurer-lapi) |
| NUXT_PUBLIC_PUBLIC_API_URL | 'https://api.agentsenintervention.fr/' | 'https://api.agentsenintervention.fr/' | ./.env.dist, ./.env | [doc](https://gitlab.com/incubateur-territoires/startups/agents-intervention/mobile#configurer-lapi) |
| TZ | Europe/Paris | Europe/Paris | docker-compose.yml | Nécessaire pour définir le fuseau horaire du conteneur. |

### Installer les dépendences

Installer les dépendences :
```shellsession
user@host mobile$ docker compose run --rm mobile npm i
```

### Configurer l'API

Pour communiquer avec l'API, il faut paramétrer son URL dans le fichier `.env`.
On peut copier le fichier exemple `.env.dist` :
```shellsession
user@host mobile$ cp .env.dist .env
```

Le fichier `.env` est ignoré par git.


## Utilisation

**Il faut démarrer les conteneurs de l'[API](https://gitlab.com/incubateur-territoires/startups/agents-intervention/api#utilisation) avant celui de cette application.**

Une fois l'installation terminée, il est possible de démarrer le conteneur avec :
```shellsession
user@host mobile$ docker compose up -d
```

L'application sera disponible dans le navigateur via : http://localhost:3000/

Pour arrêter les conteneurs :
```shellsession
user@host mobile$ docker compose down
```

### Développement

Pour ajouter des commandes/alias au conteneur via un fichier `.ashrc`,
on peut copier le fichier exemple `.ashrc.dist` :
```shellsession
user@host mobile$ cp .ashrc.dist .ashrc
```

Et ensuite, le monter dans le conteneur grâce au fichier `docker-compose.override.yml`
à créer à la racine du projet :
```yaml
services:
    mobile:
        volumes:
            - ./.ashrc:/home/node/.ashrc
```

Les fichiers `.ashrc` et `docker-compose.override.yml` sont ignorés par git.

#### Commandes/alias par défaut

Le fichier `.ashrc.dist` contient les éléments suivants :
- `update` : pour mettre les dépendances du projet à jour.


## Licence

Ce projet est sous licence [MIT](./LICENSE).
