export const getInitials = (agent: { firstname: string; lastname: string }): string => {
    const firstnameInitial = agent.firstname.charAt(0).toUpperCase();
    const lastnameInitial = agent.lastname.charAt(0).toUpperCase();
    return `${firstnameInitial}${lastnameInitial}`;
};