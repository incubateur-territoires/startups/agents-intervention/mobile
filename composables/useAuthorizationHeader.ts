export const useAuthorizationHeader = (contentType: string) => {
    return {
        "Content-Type": contentType,
        "Authorization": "Bearer "+ getToken()
    };
}
