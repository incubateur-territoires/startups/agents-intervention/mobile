export const useFetchEmployerCoordinates = async () => {
    const { baseURL } = useRuntimeConfig().app;
    const token = getToken() ?? "";
    const decodedToken = getDecodedToken(token);

    const { data: employer } = await useFetch(
        baseURL + "api/employers/" + decodedToken.employerId,
        {
            headers: useAuthorizationHeader("application/json"),
        }
    );

    return {
        longitude: employer.value.longitude,
        latitude: employer.value.latitude
    };
}
