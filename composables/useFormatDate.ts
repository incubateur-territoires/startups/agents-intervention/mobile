export const useFormatDate = (dateString: string) => {
    const options = { day: 'numeric', month: 'long', year: 'numeric' };
    const date = new Date(dateString);
    const currentDate = new Date();

    if (
        date.getDate() === currentDate.getDate() &&
        date.getMonth() === currentDate.getMonth() &&
        date.getFullYear() === currentDate.getFullYear()
    ) {
        return "aujourd'hui";
    } else if (date.getDate() === currentDate.getDate() - 1) {
        return "hier";
    } else {
        const formattedDate = date.toLocaleDateString('fr-FR', options);
        return `le ${formattedDate}`;
    }
}
