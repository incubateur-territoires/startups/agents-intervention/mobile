export const usePinPicture = (priority: string) => {
    const { baseURL } = useRuntimeConfig().app;

    if(priority === 'urgent') {
      return baseURL + 'img/map-pin-red.png';
    }
  
    return baseURL + 'img/map-pin-blue.png';
}
