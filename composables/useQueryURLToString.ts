export const useQueryURLToString = (event: Event) => {

  const queryFromUrl = getQuery(event);
  let query = "?";

  if (queryFromUrl['order[id]'] !== undefined) {
    query += "order[id]=" + queryFromUrl['order[id]'];
  }

  if (queryFromUrl['status.slug[]'] !== undefined) {

    for(const statusSlug of queryFromUrl['status.slug[]']) {
      query += "&status.slug[]=" + statusSlug;
    }
  }

  return query;
}
