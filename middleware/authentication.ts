export default defineNuxtRouteMiddleware(() => {
    const { baseURL } = useRuntimeConfig().app;
    const tokenCookie = useCookie("aei-mobile-token");

    if (tokenCookie.value === undefined) {
        return navigateTo(baseURL + 'connexion');
    }
});
