// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  css: [
    '@gouvfr/dsfr/dist/dsfr/dsfr.min.css',
    '@gouvfr/dsfr/dist/utility/icons/icons-business/icons-business.min.css',
    '@gouvfr/dsfr/dist/utility/icons/icons-document/icons-document.min.css',
    '@gouvfr/dsfr/dist/utility/icons/icons-editor/icons-editor.min.css',
    '@gouvfr/dsfr/dist/utility/icons/icons-map/icons-map.min.css',
    '@gouvfr/dsfr/dist/utility/icons/icons-media/icons-media.min.css',
    '@gouvfr/dsfr/dist/utility/icons/icons-system/icons-system.min.css',
    'assets/global.css'
  ],
  devtools: { enabled: true },
  runtimeConfig: {
    apiUrl: '',
    public: {
      publicApiUrl: ''
    }
  }
})
