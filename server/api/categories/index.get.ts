const getCategories = async function (event: Event)  {
    const { apiUrl } = useRuntimeConfig();

    const categories = await $fetch(apiUrl + 'categories', {
      method: 'GET',
      headers: {
        'content-type': 'application/json',
        'authorization': event.req.headers.authorization
      }
    })
    .catch((error) => {
      return error;
    });

  return categories["hydra:member"];
}

export default defineEventHandler(getCategories);
