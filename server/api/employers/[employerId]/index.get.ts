const getEmployer = async function(event: Event) {
  const { apiUrl } = useRuntimeConfig();
  const { employerId } = event.context.params;

  return await $fetch(apiUrl + 'employers/' + employerId, {
    method: 'GET',
    headers: {
      'content-type': 'application/json',
      'authorization': event.req.headers.authorization
    }
  });
}

export default defineEventHandler(getEmployer);
