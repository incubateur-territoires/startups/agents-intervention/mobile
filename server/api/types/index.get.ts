const getTypes = async function (event: Event) {
    const { apiUrl } = useRuntimeConfig();

    const types = await $fetch(apiUrl + 'types', {
      method: 'GET',
      headers: {
        'content-type': 'application/json',
        'authorization': event.req.headers.authorization
      }
    })
    .catch((error) => {
      return error;
    });

  return types["hydra:member"];
}

export default defineEventHandler(getTypes);
