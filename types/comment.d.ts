export default interface Comment {
    message: string;
    createdAt: string;
    author: User;
    intervention: string;
}