export default interface Group {
    name: string;
    slug: string;
}