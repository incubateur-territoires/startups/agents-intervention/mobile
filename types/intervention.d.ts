import Status from "./status";
import Priority from "./priority";
import Category from "./category";
import Type from "./type";
import User from "./user";
import Employer from "./employer";
import Location from "./location";
import Picture from "./picture";
import Comment from "./comment";

export default interface Intervention {
    id: string;
    createdAt: Date;
    description: string;
    status: Status;
    priority: Priority;
    category: Category;
    type: Type;
    author: User;
    employer: Employer;
    location: Location;
    participants: User[];
    pictures: Picture[];
    comments: Comment[];
}