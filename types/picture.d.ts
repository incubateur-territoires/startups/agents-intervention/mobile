
export default interface Picture {
    id: string;
    fileName: string;
    URL: string;
    intervention: string;
    createdAt: string;
    tags: Tag[];
}