import Category from "./category";

export default interface Type {
    id: number;
    name: string;
    picture: string;
    category: Category;
    pressed: boolean;
}