import Group from "./group";

export default interface User {
    id: number;
    firstname: string;
    lastname: string;
    email: string;
    phoneNumber: string;
    picture: string;
    groups: Group[];
}